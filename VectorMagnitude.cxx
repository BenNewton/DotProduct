#include <chrono> //for timing
#include <string.h>
#include <iomanip>
#include <fstream>

#include <vtkm/filter/VectorMagnitude.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/DataSetFieldAdd.h>

int numIterations = 1;
bool writeToFile = false, saveTimes = false, checkAnswer = false;
vtkm::Id SIDE_LENGTH = 256;
std::string fileSuffix;

//Make3DUniformDataSet0


void VectorMagnitude(bool checkAnswer, bool saveTimes, vtkm::Id SIDE_LENGTH)
{
  vtkm::Id total = 0;
  vtkm::Id times[numIterations];
  std::cout << "Runs Single Average" << std::endl;

//setup
  //vtkm::cont::testing::MakeTestDataSet testDataSet;
  //vtkm::cont::DataSet dataSet = testDataSet.Make3DUniformDataSet0();

  vtkm::cont::DataSetBuilderUniform dsb;
  vtkm::Id3 dimensions(3, 2, 3);
  vtkm::cont::DataSet dataSet = dsb.Create(dimensions);

  vtkm::cont::DataSetFieldAdd dsf;
  const int nVerts = 18;
  vtkm::Float32 vars[nVerts] = { 10.1f,  20.1f,  30.1f,  40.1f,  50.2f,  60.2f,
                                 70.2f,  80.2f,  90.3f,  100.3f, 110.3f, 120.3f,
                                 130.4f, 140.4f, 150.4f, 160.4f, 170.5f, 180.5f };

  //Set point and cell scalar
  dsf.AddPointField(dataSet, "pointvar", vars, nVerts);

  vtkm::Float32 cellvar[4] = { 100.1f, 100.2f, 100.3f, 100.4f };
  dsf.AddCellField(dataSet, "cellvar", cellvar, 4, "cells");


  ///additional
  //const int nVerts = 18;
  vtkm::Float64 fvars[nVerts] = { 10.1,  20.1,  30.1,  40.1,  50.2,  60.2,  70.2,  80.2,  90.3,
                                  100.3, 110.3, 120.3, 130.4, 140.4, 150.4, 160.4, 170.5, 180.5 };

  std::vector<vtkm::Vec<vtkm::Float64, 3>> fvec(nVerts);
  for (std::size_t i = 0; i < fvec.size(); ++i)
  {
    fvec[i] = vtkm::make_Vec(fvars[i], fvars[i], fvars[i]);
  }
  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>> finput = vtkm::cont::make_ArrayHandle(fvec);

  vtkm::cont::DataSetFieldAdd::AddPointField(dataSet, "double_vec_pointvar", finput);
//setup done


  for(vtkm::Id index = 0; index < numIterations; index++) {
    if(index > 0)
      std::cout << "\r";
    auto s = std::chrono::high_resolution_clock::now();


    vtkm::filter::Result result;
    vtkm::filter::VectorMagnitude vm;

    result = vm.Execute(dataSet, dataSet.GetField("double_vec_pointvar"));


    auto e = std::chrono::high_resolution_clock::now();
    vtkm::Id run =
      std::chrono::duration_cast<std::chrono::microseconds>(e-s).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
  }
  std::cout << std::endl;

  /* add later
if(checkAnswer) {
    vtkm::cont::ArrayHandle<vtkm::Float64> resultArrayHandle;
  bool valid = result.FieldAs(resultArrayHandle);
  if (valid)
  {
    for (vtkm::Id i = 0; i < resultArrayHandle.GetNumberOfValues(); ++i)
    {
      VTKM_TEST_ASSERT(test_equal(std::sqrt(3 * fvars[i] * fvars[i]),
                                  resultArrayHandle.GetPortalConstControl().Get(i)),
                       "Wrong result for Magnitude worklet");
    }
    }
}*/

  if(saveTimes) {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(vtkm::Id index = 0; index < numIterations; index++) {
      outfile << times[index] << '\n';
    }
  }
}

void printUsage()
{
  std::cerr << "Usage: VectorMagnitude_SERIAL [-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
    if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
    }
    else if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Vector Magnitude" << std::endl;

  VectorMagnitude(checkAnswer, saveTimes, SIDE_LENGTH);

  return 0;
}
