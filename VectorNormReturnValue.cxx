//Author: Ben Newton

// perform a vector vector normalization
// hopefully this reuses enough data to be useful

// TBB and CUDA versions will set this variable if they are present
// however if no other versions exist, we fall back to serial
#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

#include <stdlib.h>
#include <string.h>

#include <vtkm/Math.h>
#include <vtkm/VectorAnalysis.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/worklet/WorkletMapField.h>

int numIterations = 1;
bool writeToFile = false, saveTimes = false, checkAnswer = false;
vtkm::Id SIDE_LENGTH = 256;
std::string fileSuffix;

using VectorType = vtkm::Vec<vtkm::FloatDefault, 3>;
using ArrayType  = vtkm::cont::ArrayHandle<VectorType>;
using PortalType = ArrayType::PortalControl;

class VectorNormWorklet : public vtkm::worklet::WorkletMapField {
  public:
    typedef void ControlSignature(FieldIn<Vec3> vector,
                                  FieldOut<Vec3> result);
    typedef _2 ExecutionSignature(_1);
    typedef   _1 InputDomain;

    VTKM_EXEC
    VectorType operator()(const VectorType &vector) const
    {
      vtkm::FloatDefault length = sqrt((vector[0]*vector[0]) + (vector[1]*vector[1]) + (vector[2]*vector[2]));
      vtkm::FloatDefault r1 = vector[0]/length;
      vtkm::FloatDefault r2 = vector[1]/length;
      vtkm::FloatDefault r3 = vector[2]/length;
      return VectorType(r1, r2, r3);
    }
};

VTKM_CONT
void computeVectorNorms(ArrayType &vecIn, ArrayType &vecOut)
{
  VectorNormWorklet worklet;
  vtkm::worklet::DispatcherMapField<VectorNormWorklet> dispatcher(worklet);

  vtkm::Id total = 0;
  vtkm::Id times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(vtkm::Id index = 0; index < numIterations; index++) {
    if(index > 0)
      std::cout << "\r";
    auto s = std::chrono::high_resolution_clock::now();

    // actually run the thing
    dispatcher.Invoke(vecIn, vecOut);

    auto e = std::chrono::high_resolution_clock::now();
    vtkm::Id run =
      std::chrono::duration_cast<std::chrono::microseconds>(e-s).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
  }
  std::cout << std::endl;

  if(checkAnswer)
    {
    PortalType port1 = vecIn.GetPortalControl();
    PortalType port2 = vecOut.GetPortalControl();
    for(vtkm::Id index = 0; index < port1.GetNumberOfValues(); index++)
      {
      VectorType v1 = port1.Get(index);
      VectorType v2 = port2.Get(index);
      VectorType correct = vtkm::Normal(v1);
      if(fabs(v2[0]-correct[0]) > .00001 ||
         fabs(v2[1]-correct[1]) > .00001 ||
         fabs(v2[2]-correct[2]) > .00001)
        {
        std::cerr << "Normalization wrong" << std::endl;
        std::cerr << "norm of <" << v1[0] << ", " << v1[1] << ", " << v1[2] << "> =";
        std::cerr << v2 << " but should equal " << correct << std::endl;
        return;
        }
      }
    }

  if(saveTimes) {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(vtkm::Id index = 0; index < numIterations; index++) {
      outfile << times[index] << '\n';
    }
  }
}

void populateWithUnits(vtkm::cont::ArrayHandle<VectorType> &vec)
{
  PortalType port = vec.GetPortalControl();
  for(vtkm::Id index = 0; index < port.GetNumberOfValues(); index++)
  {
    float r1 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r2 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r3 = (((float) rand()) / (float) RAND_MAX)*10.0;
    port.Set(index, VectorType(r1, r2, r3));
  }
}

void printUsage()
{
  std::cerr << "Usage: VectorNorm_SERIAL [-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
    if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
    }
    else if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Vector Normalization Return Value" << std::endl;

  ArrayType vectorsIn;
  ArrayType vectorsOut;
  const vtkm::Id ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;
  vectorsIn.Allocate(ARRAY_LENGTH);
  vectorsOut.Allocate(ARRAY_LENGTH);

  populateWithUnits(vectorsIn);

  computeVectorNorms(vectorsIn, vectorsOut);

  if(writeToFile) {
    // builder to create a uniform grid
    vtkm::cont::DataSetBuilderUniform dataSetBuilder;
    vtkm::Id3 pointDims(SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
    vtkm::Id3 cellDims = pointDims - vtkm::Id3(1, 1, 1);

    // create our grid as a tall column shape
    vtkm::cont::DataSet dataSet = dataSetBuilder.Create(
        pointDims, // number of points per dimension
        VectorType(-SIDE_LENGTH/2.0, -SIDE_LENGTH/2.0, 0), // center x&y dims
        VectorType(1.0, 1.0, 1.0));

    // add to the dataset
    vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
    dataSetFieldAdd.AddPointField(dataSet, "vectorNorm", vectorsOut); //not sure this is right for floats instead of vectors?  todo

    vtkm::io::writer::VTKDataSetWriter writer("vectorNorm.vtk");
    writer.WriteDataSet(dataSet);
    }

  return 0;
}

