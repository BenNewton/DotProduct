#include <iostream>
#include <chrono>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <numeric>

int numIterations = 1;
//bool writeToFile = false,
bool saveTimes = false, checkAnswer = false;
int SIDE_LENGTH = 256;
std::string fileSuffix;

void func(float* a, float* b, float* c,
          float* d, float* e, float* f, float* o, int len)
{
  for (int i = 0; i <len; ++i)
        o[i] = a[i]*d[i] + b[i]*e[i] + c[i]*f[i];
}

void populateWithUnits(float* a, float* b, float* c,
                       float* d, float* e, float* f, int len)
{
  for(int index = 0; index < len; index++)
  {
    a[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    b[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    c[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    d[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    e[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    f[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
  }
}

void printUsage()
{
  std::cerr << "Usage: DotProductTest ["; //-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
  /*if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
      }
      else*/
  if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
  else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Dot Product Fastest" << std::endl;

  const int ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;

#if defined(__INTEL_COMPILER)
  float* a = (float*) _mm_malloc(ARRAY_LENGTH*sizeof(float), 64);
  float* b = (float*) _mm_malloc(ARRAY_LENGTH*sizeof(float), 64);
  float* c = (float*) _mm_malloc(ARRAY_LENGTH*sizeof(float), 64);
  float* d = (float*) _mm_malloc(ARRAY_LENGTH*sizeof(float), 64);
  float* e = (float*) _mm_malloc(ARRAY_LENGTH*sizeof(float), 64);
  float* f = (float*) _mm_malloc(ARRAY_LENGTH*sizeof(float), 64);
  float* o = (float*) _mm_malloc(ARRAY_LENGTH*sizeof(float), 64);
#else
  float* a = new float[ARRAY_LENGTH];  //seems to be .021ms faster (for length 60^3) from the stack (float a[ARRAY_LENGTH]) but this means -l can be at most 60
  float* b = new float[ARRAY_LENGTH];
  float* c = new float[ARRAY_LENGTH];
  float* d = new float[ARRAY_LENGTH];
  float* e = new float[ARRAY_LENGTH];
  float* f = new float[ARRAY_LENGTH];
  float* o = new float[ARRAY_LENGTH];
#endif

  populateWithUnits(a,b,c,d,e,f, ARRAY_LENGTH);

  int total = 0;
  int times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(int index = 0; index < numIterations; index++)
    {
    if(index > 0)
      std::cout << "\r";
    auto st = std::chrono::high_resolution_clock::now();
    func(a, b, c, d, e, f, o, ARRAY_LENGTH);
    auto et = std::chrono::high_resolution_clock::now();

    int run = std::chrono::duration_cast<std::chrono::microseconds>(et-st).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
    }
  std::cout << std::endl;

  if(checkAnswer)
    {
    for(int index = 0; index < ARRAY_LENGTH; index++)
      {
      float v1[] = {a[index],b[index],c[index]};
      float v2[] = {d[index],e[index],f[index]};
      float correct = std::inner_product(v1,v1+3,v2,0.0);
      if(o[index]-correct > .0001 || o[index]-correct < -.0001)
        {
        std::cerr << "Dot product wrong" << std::endl;
        std::cerr << "<" << v1[0] << ", " << v1[1] << ", " << v1[2] << "> dot ";
        std::cerr << "<" << v2[0] << ", " << v2[1] << ", " << v2[2] << "> = ";
        std::cerr << o[index] << " but should equal " << correct << std::endl;
        return 1;
      }
    }
    }

  if(saveTimes)
    {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(int index = 0; index < numIterations; index++)
      {
      outfile << times[index] << '\n';
      }
    }

  return 0;
}
