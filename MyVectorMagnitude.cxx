//Author: Ben Newton

// perform a vector magnitude calc
// hopefully this reuses enough data to be useful

// TBB and CUDA versions will set this variable if they are present
// however if no other versions exist, we fall back to serial
#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

#include <stdlib.h>
#include <string.h>

#include <vtkm/Math.h>
#include <vtkm/VectorAnalysis.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/Magnitude.h>

int numIterations = 1;
bool writeToFile = false, saveTimes = false, checkAnswer = false;
vtkm::Id SIDE_LENGTH = 256;
std::string fileSuffix;

using VectorType = vtkm::Vec<vtkm::FloatDefault, 3>;
using ArrayVectorType  = vtkm::cont::ArrayHandle<VectorType>;
using ArrayReturnType  = vtkm::cont::ArrayHandle<vtkm::FloatDefault>;
using PortalVectorType = ArrayVectorType::PortalControl;
using PortalReturnType = ArrayReturnType::PortalControl;

class MyMagnitudeWorklet : public vtkm::worklet::WorkletMapField {
  public:
    typedef void ControlSignature(FieldIn<Vec3> vector,
                                  FieldOut<vtkm::FloatDefault> result);
    typedef void ExecutionSignature(_1, _2);
    typedef   _1 InputDomain;

    VTKM_EXEC
    void operator()(const VectorType &vector,
                    vtkm::FloatDefault &result) const {
      result = std::sqrt(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]);
    }
};


VTKM_CONT
void computeVectorMagnitudes(ArrayVectorType &vecIn, ArrayReturnType &result)
{
  //below 2 lines fail
  //vtkm::worklet::Magnitude magnitudeWorklet;
  //vtkm::worklet::DispatcherMapField<vtkm::worklet::Magnitude> dispatcher(magnitudeWorklet);

  //below hopefully works
  MyMagnitudeWorklet worklet;
  vtkm::worklet::DispatcherMapField<MyMagnitudeWorklet> dispatcher(worklet);

  vtkm::Id total = 0;
  vtkm::Id times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(vtkm::Id index = 0; index < numIterations; index++) {
    if(index > 0)
      std::cout << "\r";
    auto s = std::chrono::high_resolution_clock::now();

    // actually run the thing
    dispatcher.Invoke(vecIn, result);

    auto e = std::chrono::high_resolution_clock::now();
    vtkm::Id run =
      std::chrono::duration_cast<std::chrono::microseconds>(e-s).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
  }
  std::cout << std::endl;

  if(checkAnswer)
    {
    PortalVectorType port1 = vecIn.GetPortalControl();
    PortalReturnType port2 = result.GetPortalControl();
    for(vtkm::Id index = 0; index < port1.GetNumberOfValues(); index++)
      {
      VectorType v1 = port1.Get(index);
      vtkm::FloatDefault v2 = port2.Get(index);
      //VectorType correct = vtkm::Normal(v1);
      vtkm::FloatDefault correct = std::sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
      if(fabs(v2-correct) > .00001)
        {
        std::cerr << "Magnitude wrong" << std::endl;
        std::cerr << "magnitude of <" << v1[0] << ", " << v1[1] << ", " << v1[2] << "> =";
        std::cerr << v2 << " but should equal " << correct << std::endl;
        return;
        }
      }
    }

  if(saveTimes) {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(vtkm::Id index = 0; index < numIterations; index++) {
      outfile << times[index] << '\n';
    }
  }
}

void populateWithUnits(vtkm::cont::ArrayHandle<VectorType> &vec)
{
  PortalVectorType port = vec.GetPortalControl();
  for(vtkm::Id index = 0; index < port.GetNumberOfValues(); index++)
  {
    float r1 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r2 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r3 = (((float) rand()) / (float) RAND_MAX)*10.0;
    port.Set(index, VectorType(r1, r2, r3));
  }
}

void printUsage()
{
  std::cerr << "Usage: VectorMagnitude_SERIAL [-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
    if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
    }
    else if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Vector Magnitude" << std::endl;

  ArrayVectorType vectorsIn;
  ArrayReturnType result;
  const vtkm::Id ARRAY_LENGTH = (SIDE_LENGTH/1)*(SIDE_LENGTH/1)*(SIDE_LENGTH/1); //can change factor ??
  vectorsIn.Allocate(ARRAY_LENGTH);
  result.Allocate(ARRAY_LENGTH);

  populateWithUnits(vectorsIn);

  computeVectorMagnitudes(vectorsIn, result);

  if(writeToFile) {
    // builder to create a uniform grid
    vtkm::cont::DataSetBuilderUniform dataSetBuilder;
    vtkm::Id3 pointDims(SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
    vtkm::Id3 cellDims = pointDims - vtkm::Id3(1, 1, 1);

    // create our grid as a tall column shape
    vtkm::cont::DataSet dataSet = dataSetBuilder.Create(
        pointDims, // number of points per dimension
        VectorType(-SIDE_LENGTH/2.0, -SIDE_LENGTH/2.0, 0), // center x&y dims
        VectorType(1.0, 1.0, 1.0));

    // add to the dataset
    vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
    dataSetFieldAdd.AddPointField(dataSet, "vectorMagnitude", result); //not sure this is right for floats instead of vectors?  todo

    vtkm::io::writer::VTKDataSetWriter writer("vectorMagnitude.vtk");
    writer.WriteDataSet(dataSet);
    }

  return 0;
}

