#include <iostream>
#include <chrono>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <numeric>
#include <math.h>

int numIterations = 1;
bool saveTimes = false, checkAnswer = false;
int SIDE_LENGTH = 256;
std::string fileSuffix;

void func(float* ax, float* ay, float* az,
          float* rx, float* ry, float* rz, int len)
{
  for (int i = 0; i <len; ++i)
    {
    float length = sqrt((ax[i]*ax[i]) + (ay[i]*ay[i]) + (az[i]*az[i]));
    rx[i] = ax[i]/length;
    ry[i] = ay[i]/length;
    rz[i] = az[i]/length;
    }
}

void populateWithUnits(float* a, float* b, float* c,
                       int len)
{
  for(int index = 0; index < len; index++)
  {
    a[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    b[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    c[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
  }
}

void printUsage()
{
  std::cerr << "Usage: VectorNormFastest ["; //-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
  /*if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
      }
      else*/
  if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
  else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Vector Normalization Fastest" << std::endl;

  const int ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;

  float* ax = new float[ARRAY_LENGTH];  //seems to be .021ms faster (for length 60^3) from the stack (float a[ARRAY_LENGTH]) but this means -l can be at most 60
  float* ay = new float[ARRAY_LENGTH];
  float* az = new float[ARRAY_LENGTH];
  float* rx = new float[ARRAY_LENGTH];
  float* ry = new float[ARRAY_LENGTH];
  float* rz = new float[ARRAY_LENGTH];

  populateWithUnits(ax, ay, az, ARRAY_LENGTH);

  int total = 0;
  int times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(int index = 0; index < numIterations; index++)
    {
    if(index > 0)
      std::cout << "\r";
    auto st = std::chrono::high_resolution_clock::now();
    func(ax, ay, az, rx, ry, rz, ARRAY_LENGTH);
    auto et = std::chrono::high_resolution_clock::now();

    int run = std::chrono::duration_cast<std::chrono::microseconds>(et-st).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
    }
  std::cout << std::endl;

  if(checkAnswer)
    {
    for(int index = 0; index < ARRAY_LENGTH; index++)
      {
      float v1[] = {ax[index],ay[index],az[index]};
      float len = sqrt(std::inner_product(v1,v1+3,v1,0.0));
      float cx = ax[index]/len;
      float cy = ay[index]/len;
      float cz = az[index]/len;

      if(fabs(rx[index]-cx) > .0001 ||
         fabs(ry[index]-cy) > .0001 ||
         fabs(rz[index]-cz) > .0001)
        {
        std::cerr << "vector norm wrong" << std::endl;
        std::cerr << " vec norm of <" << ax[index] << ", " << ay[index] << ", " << az[index] << "> is";
        std::cerr << " < " << rx[index] << ", " << ry[index] << ", " << rz[index] << ">  but should equal <" << cx << ", " << cy << ", " << cz  << " > " << std::endl;
        return 1;
        }
      }
    }

  if(saveTimes)
    {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(int index = 0; index < numIterations; index++)
      {
      outfile << times[index] << '\n';
      }
    }

  return 0;
}
