#include <chrono> //for timing
#include <string.h>
#include <iomanip>
#include <fstream>

//#include <vtkm/Math.h>
//#include <vtkm/cont/ArrayHandleUniformPointCoordinates.h>
//#include <vtkm/cont/CellSetSingleType.h>
//#include <vtkm/cont/DataSet.h>
//#include <vtkm/cont/DynamicArrayHandle.h>
//#include <vtkm/cont/testing/Testing.h>

//#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/MarchingCubes.h>

int numIterations = 1;
bool writeToFile = false, saveTimes = false, checkAnswer = false;
vtkm::Id SIDE_LENGTH = 256;
std::string fileSuffix;


/*class TangleField : public vtkm::worklet::WorkletMapField
{
public:
  typedef void ControlSignature(FieldIn<IdType> vertexId, FieldOut<Scalar> v);
  typedef void ExecutionSignature(_1, _2);
  typedef _1 InputDomain;

  const vtkm::Id xdim, ydim, zdim;
  const vtkm::FloatDefault xmin, ymin, zmin, xmax, ymax, zmax;
  const vtkm::Id cellsPerLayer;

  VTKM_CONT
  TangleField(const vtkm::Id3 dims,
              const vtkm::FloatDefault mins[3],
              const vtkm::FloatDefault maxs[3])
    : xdim(dims[0])
    , ydim(dims[1])
    , zdim(dims[2])
    , xmin(mins[0])
    , ymin(mins[1])
    , zmin(mins[2])
    , xmax(maxs[0])
    , ymax(maxs[1])
    , zmax(maxs[2])
    , cellsPerLayer((xdim) * (ydim))
  {
  }

  VTKM_EXEC
  void operator()(const vtkm::Id& vertexId, vtkm::Float32& v) const
  {
    const vtkm::Id x = vertexId % (xdim);
    const vtkm::Id y = (vertexId / (xdim)) % (ydim);
    const vtkm::Id z = vertexId / cellsPerLayer;

    const vtkm::FloatDefault fx =
      static_cast<vtkm::FloatDefault>(x) / static_cast<vtkm::FloatDefault>(xdim - 1);
    const vtkm::FloatDefault fy =
      static_cast<vtkm::FloatDefault>(y) / static_cast<vtkm::FloatDefault>(xdim - 1);
    const vtkm::FloatDefault fz =
      static_cast<vtkm::FloatDefault>(z) / static_cast<vtkm::FloatDefault>(xdim - 1);

    const vtkm::Float32 xx = 3.0f * vtkm::Float32(xmin + (xmax - xmin) * (fx));
    const vtkm::Float32 yy = 3.0f * vtkm::Float32(ymin + (ymax - ymin) * (fy));
    const vtkm::Float32 zz = 3.0f * vtkm::Float32(zmin + (zmax - zmin) * (fz));

    v = (xx * xx * xx * xx - 5.0f * xx * xx + yy * yy * yy * yy - 5.0f * yy * yy +
         zz * zz * zz * zz - 5.0f * zz * zz + 11.8f) *
        0.2f +
      0.5f;
  }
  };*/

//copied from UnitTestMarchingCubes
vtkm::cont::DataSet MakeIsosurfaceTestDataSet(vtkm::Id3 dims)
{
  //just duplicated the tangle field worklet here instead of using it, because on Bowman with ivdep enabled and Release build (optimizations) enabled this worklet segfaults.
  vtkm::cont::DataSet dataSet;

  const vtkm::Id3 vdims(dims[0] + 1, dims[1] + 1, dims[2] + 1);

  vtkm::FloatDefault mins[3] = { -1.0f, -1.0f, -1.0f };
  vtkm::FloatDefault maxs[3] = { 1.0f, 1.0f, 1.0f };

  vtkm::cont::ArrayHandle<vtkm::Float32> pointFieldArray;
  //vtkm::cont::ArrayHandleIndex vertexCountImplicitArray(vdims[0] * vdims[1] * vdims[2]);
  /*vtkm::worklet::DispatcherMapField<TangleField> tangleFieldDispatcher(
    TangleField(vdims, mins, maxs));
    tangleFieldDispatcher.Invoke(vertexCountImplicitArray, pointFieldArray);*/

  vtkm::Id xdim, ydim, zdim;
  vtkm::FloatDefault xmin, ymin, zmin, xmax, ymax, zmax;
  vtkm::Id cellsPerLayer;
  xdim = vdims[0];
  ydim = vdims[1];
  zdim = vdims[2];
  xmin = mins[0];
  ymin = mins[1];
  zmin = mins[2];
  xmax = maxs[0];
  ymax = maxs[1];
  zmax = maxs[2];
  cellsPerLayer = (xdim) * (ydim);

  int count = vdims[0] * vdims[1] * vdims[2];
  pointFieldArray.Allocate(count);
  vtkm::cont::ArrayHandle<vtkm::Float32>::PortalControl port1 = pointFieldArray.GetPortalControl();

  for(int vertexId=0; vertexId<count; vertexId++)
    {
    vtkm::Id x = vertexId % (xdim);
    vtkm::Id y = (vertexId / (xdim)) % (ydim);
    vtkm::Id z = vertexId / cellsPerLayer;

    vtkm::FloatDefault fx =
      static_cast<vtkm::FloatDefault>(x) / static_cast<vtkm::FloatDefault>(xdim - 1);
    vtkm::FloatDefault fy =
      static_cast<vtkm::FloatDefault>(y) / static_cast<vtkm::FloatDefault>(xdim - 1);
    vtkm::FloatDefault fz =
      static_cast<vtkm::FloatDefault>(z) / static_cast<vtkm::FloatDefault>(xdim - 1);

    vtkm::Float32 xx = 3.0f * vtkm::Float32(xmin + (xmax - xmin) * (fx));
    vtkm::Float32 yy = 3.0f * vtkm::Float32(ymin + (ymax - ymin) * (fy));
    vtkm::Float32 zz = 3.0f * vtkm::Float32(zmin + (zmax - zmin) * (fz));

    vtkm::Float32 v = (xx * xx * xx * xx - 5.0f * xx * xx + yy * yy * yy * yy - 5.0f * yy * yy +
         zz * zz * zz * zz - 5.0f * zz * zz + 11.8f) *
        0.2f +
      0.5f;
    port1.Set(vertexId, v);
    }

  /*std::cout << "--------" <<std::endl;
  for(int i=0; i<port1.GetNumberOfValues(); i++)
    {
    std::cout << port1.Get(i) << std::endl;
    }
    std::cout << "--------" <<std::endl;*/

  vtkm::Id numCells = dims[0] * dims[1] * dims[2];
  auto cellFieldArray = vtkm::cont::make_ArrayHandleCounting<vtkm::Id>(0, 1, numCells);

  vtkm::Vec<vtkm::FloatDefault, 3> origin(0.0f, 0.0f, 0.0f);
  vtkm::Vec<vtkm::FloatDefault, 3> spacing(1.0f / static_cast<vtkm::FloatDefault>(dims[0]),
                                           1.0f / static_cast<vtkm::FloatDefault>(dims[2]),
                                           1.0f / static_cast<vtkm::FloatDefault>(dims[1]));

  vtkm::cont::ArrayHandleUniformPointCoordinates coordinates(vdims, origin, spacing);
  dataSet.AddCoordinateSystem(vtkm::cont::CoordinateSystem("coordinates", coordinates));

  static const vtkm::IdComponent ndim = 3;
  vtkm::cont::CellSetStructured<ndim> cellSet("cells");
  cellSet.SetPointDimensions(vdims);
  dataSet.AddCellSet(cellSet);

  dataSet.AddField(vtkm::cont::Field("nodevar", vtkm::cont::Field::ASSOC_POINTS, pointFieldArray));
  dataSet.AddField(
    vtkm::cont::Field("cellvar", vtkm::cont::Field::ASSOC_CELL_SET, "cells", cellFieldArray));

  return dataSet;
}

//copied from UnitTestMarchingCubes
void MarchingCubesUniformGrid(bool checkAnswer, bool saveTimes, vtkm::Id SIDE_LENGTH)
{
  //std::cout << "Testing MarchingCubes filter on a uniform grid" << std::endl;
  vtkm::Id total = 0;
  vtkm::Id times[numIterations];
  std::cout << "Runs Single Average" << std::endl;

  vtkm::Id3 dims(SIDE_LENGTH/4, SIDE_LENGTH/4, SIDE_LENGTH/4); //dims(4, 4, 4);
  vtkm::cont::DataSet dataSet = MakeIsosurfaceTestDataSet(dims);
  typedef VTKM_DEFAULT_DEVICE_ADAPTER_TAG DeviceAdapter;

  for(vtkm::Id index = 0; index < numIterations; index++) {
    if(index > 0)
      std::cout << "\r";
    auto s = std::chrono::high_resolution_clock::now();

    vtkm::cont::CellSetStructured<3> cellSet;
    dataSet.GetCellSet().CopyTo(cellSet);
    vtkm::cont::ArrayHandle<vtkm::Float32> pointFieldArray;
    dataSet.GetField("nodevar").GetData().CopyTo(pointFieldArray);
    vtkm::cont::ArrayHandleCounting<vtkm::Id> cellFieldArray;
    dataSet.GetField("cellvar").GetData().CopyTo(cellFieldArray);

    vtkm::worklet::MarchingCubes isosurfaceFilter;
    isosurfaceFilter.SetMergeDuplicatePoints(false);

    vtkm::Float32 contourValue = 0.5f;
    vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3>> verticesArray;
    vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3>> normalsArray;
    vtkm::cont::ArrayHandle<vtkm::Float32> scalarsArray;

    auto result = isosurfaceFilter.Run(&contourValue,
                                       1,
                                       cellSet,
                                       dataSet.GetCoordinateSystem(),
                                       pointFieldArray,
                                       verticesArray,
                                       normalsArray,
                                       DeviceAdapter());

    scalarsArray = isosurfaceFilter.ProcessPointField(pointFieldArray, DeviceAdapter());

    vtkm::cont::ArrayHandle<vtkm::Id> cellFieldArrayOut;
    cellFieldArrayOut = isosurfaceFilter.ProcessCellField(cellFieldArray, DeviceAdapter());

  /*std::cout << "vertices: ";
  vtkm::cont::printSummary_ArrayHandle(verticesArray, std::cout);
  std::cout << std::endl;
  std::cout << "normals: ";
  vtkm::cont::printSummary_ArrayHandle(normalsArray, std::cout);
  std::cout << std::endl;
  std::cout << "scalars: ";
  vtkm::cont::printSummary_ArrayHandle(scalarsArray, std::cout);
  std::cout << std::endl;
  std::cout << "cell field: ";
  vtkm::cont::printSummary_ArrayHandle(cellFieldArrayOut, std::cout);
  std::cout << std::endl;*/

    /*   if(checkAnswer)
      {
      //VTKM_TEST_ASSERT(result.GetNumberOfCells() == cellFieldArrayOut.GetNumberOfValues(),
      if(result.GetNumberOfCells() != cellFieldArrayOut.GetNumberOfValues())
        {
        std::cout << "Output cell data invalid" << std::endl;
        }

      //VTKM_TEST_ASSERT(test_equal(verticesArray.GetNumberOfValues(), 480),
      if(verticesArray.GetNumberOfValues() != 480)
        {
        std::cout << "Wrong result for Isosurface filter" << std::endl;
        }
        }*/ //todo move

    auto e = std::chrono::high_resolution_clock::now();
    vtkm::Id run =
      std::chrono::duration_cast<std::chrono::microseconds>(e-s).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
  }
  std::cout << std::endl;

  if(saveTimes) {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(vtkm::Id index = 0; index < numIterations; index++) {
      outfile << times[index] << '\n';
    }
  }
}


void printUsage()
{
  std::cerr << "Usage: MarchingCubes_SERIAL [-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
    if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
    }
    else if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Marching Cubes" << std::endl;

  MarchingCubesUniformGrid(checkAnswer, saveTimes, SIDE_LENGTH);

  /*ArrayType vectorsIn1;
  ArrayType vectorsIn2;
  OutputArrayType vectorsOut;
  const vtkm::Id ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;
  vectorsIn1.Allocate(ARRAY_LENGTH);
  vectorsIn2.Allocate(ARRAY_LENGTH);
  vectorsOut.Allocate(ARRAY_LENGTH);

  // fill with <1, 0, 0> and <0, 1, 0>
  populateWithUnits(vectorsIn1, vectorsIn2);

  computeDotProducts(vectorsIn1, vectorsIn2, vectorsOut);

  if(writeToFile) {
    // builder to create a uniform grid
    vtkm::cont::DataSetBuilderUniform dataSetBuilder;
    vtkm::Id3 pointDims(SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
    vtkm::Id3 cellDims = pointDims - vtkm::Id3(1, 1, 1);

    // create our grid as a tall column shape
    vtkm::cont::DataSet dataSet = dataSetBuilder.Create(
        pointDims, // number of points per dimension
        VectorType(-SIDE_LENGTH/2.0, -SIDE_LENGTH/2.0, 0), // center x&y dims
        VectorType(1.0, 1.0, 1.0));

    // add to the dataset
    vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
    dataSetFieldAdd.AddPointField(dataSet, "dotProduct", vectorsOut); //not sure this is right for floats instead of vectors?  todo

    vtkm::io::writer::VTKDataSetWriter writer("dotProduct.vtk");
    writer.WriteDataSet(dataSet);
    }*/

  return 0;
}
